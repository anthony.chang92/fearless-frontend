import { NavLink } from "react-router-dom"

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">Conference GO!</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <NavLink className="nav-link" aria-current="page" to="/">
                Home
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/locations/new">
                New location
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/conferences/new" >
                New conference
              </NavLink>
              <NavLink aria-current="page" to="/presentations/new" >
                New presentation
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/attendees/new" >
                New Attendee
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/attendees" >
                Attendee list
              </NavLink>
          </div>
        </div>
      </nav>
    );
  }

  export default Nav;
